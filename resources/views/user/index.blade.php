@extends('layouts.app')
@section('title','Create User')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Users</div>

                <div class="card-body">

                {{ session('status') }}

                {{-- <table border="1"> --}}
                <table class="table table-bordered">
                    <tr>
                        <th>NO.</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th colspan="2"></th>
                    </tr>
                    @foreach($users as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>
                            {{ link_to('user/'.$row->id.'/edit','Edit',['class'=>'btn btn-info']) }}
                        </td>
                        <td>    
                            {{ Form::open(['url'=>'user/'.$row->id,'method'=>'delete']) }}
                                {{-- {{ Form::submit('Delete',['class'=>'btn btn-warning']) }} --}}
                                <button type="submit" onClick="return confirm('serius?')">Delete</button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </table>
                
                {{-- untuk halaman --}}
                {{ $users->links() }}
                {{-- untuk halaman --}}

                {{-- {{ link_to('employee/create','Create New') }} --}}
                {{ link_to('user/create','Create New',['class'=>'btn btn-danger']) }}

                </div>
            </div>
        </div>
    </div>
</div>

    @endsection