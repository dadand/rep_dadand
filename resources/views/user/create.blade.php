@extends('layouts.app')
@section('title','Create User')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data User</div>

                <div class="card-body">

                    <h3>Form Create User</h3>

                    @include ('validation-error')

                    {{ Form::open(['url'=>'user']) }}

                    @include ('user.form')

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection