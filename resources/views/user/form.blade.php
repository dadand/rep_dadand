<table class="table table-bordered">
    <tr>
        <td>NAME</td>
        <td>{{ Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td>EMAIL</td>
        <td>{{ Form::text('email',null,['placeholder'=>'Email','class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td>PASSWORD</td>
        <td>{{ Form::password('password',['placeholder'=>'Password','class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td></td>
        <td>
            {{ Form::submit('Save Data',['class'=>'btn btn-success']) }}
            {{ link_to('user','Back',['class'=>'btn btn-warning']) }}
        </td>
    </tr>
</table>
