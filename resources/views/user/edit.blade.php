@extends('layouts.app')
@section('title','Update User')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Users</div>

                <div class="card-body">

                    <h3>Form Edit Users</h3>

                    @include ('validation-error')

                    {{ Form::model($user,['url'=>'user/'.$user->id,'method'=>'PUT']) }}

                    @include ('user.form')

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection