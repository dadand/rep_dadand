@extends('template')
@section('title','Update Department')
@section('content')

    <h3>Form Edit Department</h3>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ Form::model($department,['url'=>'department/'.$department->nik,'method'=>'PUT']) }}
    <table>
        <tr>
            <td>Name</td>
            <td>{{ Form::text('nama_department',null,['placeholder'=>'nama_department']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>
                {{ Form::submit('Save Data') }}
                {{ link_to('department','Back') }}
            </td>
        </tr>
    </table>
    {{ Form::close() }}
@endsection