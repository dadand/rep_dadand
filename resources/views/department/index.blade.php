@extends('template')
@section('title','Create Department')
@section('content')

    {{ session('status') }}

    <table border="1">
        <tr>
            <th>NO.</th>
            <th>ID.</th>
            <th>DEPARTMENT NAME</th>
            <th colspan="2">MODIF</th>
        </tr>
        @foreach($department as $row)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $row->id }}</td>
            <td>{{ $row->nama_department }}</td>
            <td>
                {{ link_to('department/'.$row->id.'/edit','Edit') }}
            </td>
            <td>    
                {{ Form::open(['url'=>'department/'.$row->id,'method'=>'delete']) }}
                    {{ Form::submit('delete') }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </table>
    
    {{ link_to('department/create','Create New') }}

    @endsection