@extends('template')
@section('title','Create Department')
@section('content')

    <h3>Form Create Department</h3>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ Form::open(['url'=>'department']) }}
    <table>
        {{-- <tr>
            <td>ID</td>
            <td>{{ Form::text('id',null,['placeholder'=>'id']) }}</td>
        </tr> --}}
        <tr>
            <td>Name</td>
            <td>{{ Form::text('nama_department',null,['placeholder'=>'Name']) }}</td>
        </tr>
        <tr>
            <td></td>
            <td>
                {{ Form::submit('Save Data') }}
                {{ link_to('department','Back') }}
            </td>
        </tr>
    </table>
    {{ Form::close() }}
@endsection