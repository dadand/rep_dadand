<html>
    <head>
        <title>Daftar Artikel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-bottom:12px;">
                    <div class="row">
                        <div class="col-md-2">
                        <img src="{{ asset('gambarnya/tol.jpeg') }}" width="170">
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="/article/1"> {{ $articlenya['title']}} </a>
                                </div>
                                <div class="col-md-12">
                                {{ $articlenya['publish_date']}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Bagian Komentar --}}
            <h3>Komentarnya dari Netizen</h3>
            <div class="row">
                <div class="col-md-2">
                    Senin, 2019-01-01
                </div>
                <div class="col-md-9" style="margin-bottom:16px;">
                    Artikelnya sangat bermanfaat
                </div>
            </div>
            <h5>Kirim Komentar</h5>
            <div class="row">
                <div class="col-md-6" style="margin-bottom:12px;">
                    {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Your Name'])}}
                </div>
                <div class="col-md-6" style="margin-bottom:12px;">
                    {{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Your Email'])}}
                </div>
                <div class="col-md-12" style="margin-bottom:12px;">
                    {{ Form::textarea('comment',null,['class'=>'form-control','placeholder'=>'Your Comment'])}}
                </div>
                <div class="col-md-12" style="margin-bottom:12px;">
                    {{ Form::submit('Send comment',['class'=>'btn btn-danger'])}}
                </div>
            </div>
        </div>
    </body>
</html>

