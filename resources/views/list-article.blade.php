<html>
    <head>
        <title>Daftar Artikel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                @foreach($articles as $articlenya)
                <div class="col-md-12" style="margin-bottom:12px;">
                    <div class="row">
                        <div class="col-md-2">
                        <!-- GAMBARNYA -->
                        <img src="{{ asset('gambarnya/tol.jpeg') }}" width="170">
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-12">
                                <!-- TITLENYA -->
                                    <a href="/article/1"> {{ $articlenya['title']}} </a>
                                </div>
                                <div class="col-md-12">
                                <!-- PUBLISHDATENYA -->
                                {{ $articlenya['publish_date']}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </body>
</html>




{{-- <!-- @foreach($articles as $articlenya)
    {{ $articlenya['title']}}
@endforeach --> --}}