@extends('layouts.app')
@section('title','Update Employee')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Employees</div>

                <div class="card-body">

                    <h3>Form Edit Employee</h3>

                    @include ('validation-error')

                    {{ Form::model($employee,['url'=>'employee/'.$employee->nik,'method'=>'PUT']) }}

                    @include ('employee.form')

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection