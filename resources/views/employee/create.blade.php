@extends('layouts.app')
@section('title','Create Employee')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Employees</div>

                <div class="card-body">

                    <h3>Form Create Employee</h3>

                    @include ('validation-error')

                    {{ Form::open(['url'=>'employee']) }}

                    @include ('employee.form')

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection