{{-- @extends('template') ganti pake layout --}}
@extends('layouts.app')
@section('title','Create Employee')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Employees</div>

                <div class="card-body">

                {{ session('status') }}

                {{-- bikin tombol export --}}
                <a href="/employee/excel" class="btn btn-lg btn-success">Export Excel</a>
                <a href="/employee/pdf" class="btn btn-sm btn-primary">Export PDF</a>
                <hr>
                
                {{ Form::open(['url'=>'employee','method'=>'GET'])}}
                <table class="table table-bordered">
                    <tr>
                        <td>Depatment</td>
                        <div class ="row">
                            <td>
                                <div class="col-md-6">
                                    {{ Form::select('department_id',$departments,$parameter,['class'=>'form-control','placeholder'=>'All Data']) }}
                                </div>
                            </td>
                            <td>
                                <div class="col-md-6">
                                    {{ Form::submit('Filter Data',['class'=>'btn btn-info'])}}
                                </div>
                            </td>
                        </div>                        
                    </tr>
                </table>
                {{ Form::close()}}

                {{-- <table border="1"> --}}
                <table class="table table-bordered">
                    <tr>
                        <th>NO.</th>
                        <th>NIK</th>
                        <th>NAME</th>
                        <th>DEPARTMENT</th>
                        <th colspan="2"></th>
                    </tr>
                    @foreach($employees as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->nik }}</td>
                        <td>{{ $row->name }}</td>
                        {{-- <td>{{ $row->nama_department }}</td> --}}
                        <td>{{ $row->department->nama_department }}</td>
                        <td>
                            {{-- {{ link_to('employee/'.$row->nik.'/edit','Edit') }} --}}
                            {{ link_to('employee/'.$row->nik.'/edit','Edit',['class'=>'btn btn-info']) }}
                        </td>
                        <td>    
                            {{ Form::open(['url'=>'employee/'.$row->nik,'method'=>'delete']) }}
                                {{-- {{ Form::submit('delete') }} --}}
                                {{ Form::submit('Delete',['class'=>'btn btn-warning']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </table>
                
                {{-- untuk halaman --}}
                {{ $employees->links() }}
                {{-- untuk halaman --}}

                {{-- {{ link_to('employee/create','Create New') }} --}}
                {{ link_to('employee/create','Create New',['class'=>'btn btn-danger']) }}

                </div>
            </div>
        </div>
    </div>
</div>

    @endsection