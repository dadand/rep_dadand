<table class="table table-bordered">
    <tr>
        <td>NIK</td>
        <td>{{ Form::text('nik',null,['placeholder'=>'NIK','class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td>Name</td>
        <td>{{ Form::text('name',null,['placeholder'=>'Name','class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td>Department</td>
        {{-- <td>{{ Form::select('department_id',[1=>'Finance',2=>'HRD'],null) }}</td> --}}
        <td>{{ Form::select('department_id',$departments,null,['class'=>'form-control']) }}</td>
    </tr>
    <tr>
        <td></td>
        <td>
            {{ Form::submit('Save Data',['class'=>'btn btn-success']) }}
            {{ link_to('employee','Back',['class'=>'btn btn-warning']) }}
        </td>
    </tr>
</table>
