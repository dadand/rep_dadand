<?php

namespace App\Exports;

use App\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeeExport implements FromView
{
    public function view(): View    
    {
        $data['employee'] = Employee::join('departments','employees.department_id','=','departments.id')
                        ->select('employees.nik','employees.name','departments.nama_department')
                        ->get();
        return view('employee.excel',$data);
        // return Employee::all();
    }
}

// class EmployeeExport implements FromCollection
// {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     public function collection()
//     {
//         return Employee::all();
//     }
// }
