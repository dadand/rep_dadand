<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $primaryKey='nik';

    //supaya tidak auto increment
    public $incrementing = false;

    function department()
    {
        return $this->belongsTo('App\Department');
    }
}
