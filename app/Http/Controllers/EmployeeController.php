<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

//memanggil model (database)
use App\Employee;
use App\Department;

//manggil excel
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    //harus login user dulu
    function __construct()
    {
        return $this->middleware('auth');
    }

    //menampilkan data karyawan
    function index()
    {
        // return view('employee.index');
        // $data['employees'] = Employee::all();
        // return view('employee.index',$data);

        // $data['employees']=Employee::join('departments','employees.department_id','=','departments.id')
        //                 ->select('employees.nik','employees.name','departments.nama_department')
        //                 ->get();
        
        // $data['employees'] = Employee::all();
        
        // $data['employees'] = Employee::paginate(2); //menampilkan halaman
        // $data['departments'] = Department::pluck('nama_department','id');
        // return view('employee.index',$data);

        $parameter = Input::get('department_id');
        //dd($parameter);
        if($parameter==null)
        {
            $data['employees'] = Employee::paginate(3); //menampilkan halaman
        }else
        {
            $data['employees'] = Employee::where('department_id',$parameter)->paginate(5);
        }
        $data['departments'] = Department::pluck('nama_department','id');
        $data['parameter'] = $parameter;
        return view('employee.index',$data);
    }

    //untuk form input data karywan
    function create()
    {
        // return view('employee.create'); arahkan combo department
        $data['departments'] = Department::pluck('nama_department','id');
        return view('employee.create',$data);
    }

    //untuk simpan data karyawan
    function store(Request $request)
    {
        // $request->validate([
        //     'nik' => 'required',
        //     'name' => 'required|min:5',
        // ]);

        $messages = [
            'required' => 'The :attribute field is required yaaaa.',
        ];
        
        $rules = [
            'nik' => 'required',
            'name' => 'required|min:5',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        
        //return $request->all(); GAK JADI

        //insert ke database
        $employee                   = new Employee;
        $employee->nik              = $request->nik;
        $employee->name             = $request->name;
        $employee->department_id    = $request->department_id;
        $employee->save();

        // return redirect('employee');
        return redirect('employee')->with('status','A New Data Employee has created');
    }

    //untuk menampilkan form edit karyawan
    function edit($nik)
    {
        $data['employee'] = Employee::where('nik',$nik)->first();
        return view('employee.edit',$data);
    }

    //untuk menampilkan update
    function update(Request $request,$nik)
    {

        $messages = [
            'required' => 'The :attribute field is required yaaaa.',
        ];
        
        $rules = [
            'nik' => 'required',
            'name' => 'required|min:5',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        
        //return $request->all(); GAK JADI

        //insert ke database
        $employee                   = Employee::where('nik',$nik)->first();
        $employee->nik              = $request->nik;
        $employee->name             = $request->name;
        $employee->department_id    = $request->department_id;
        $employee->update();

        // return redirect('employee');
        return redirect('employee')->with('status','A Data Employee has Updated');

    }

    //ngapus
    function destroy($nik)
    {
        $employee   = Employee::where('nik',$nik)->first();
        $employee->delete();
        return redirect('employee')->with('status','A Data Employee Name '.$employee->name.' has DELETED');
    }

    function excel()
    {
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }

    function pdf()
    {
        $data['employees'] = Employee::join('departments','employees.department_id','=','departments.id')
                        ->select('employees.nik','employees.name','departments.nama_department')
                        ->get();
        $pdf = \PDF::loadView('employee/pdf', $data);
        // return $pdf->stream('laporan-karyawan.pdf');
        return $pdf->download('laporan-karyawan.pdf');
        
    }    
}
?>