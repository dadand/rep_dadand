<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

//memanggil model (database)
use App\Department;

class DepartmentController extends Controller
{
    //menampilkan data dept.
    function index()
    {
        //return view('department.index');
         $data['department'] = Department::all();
         return view('department.index',$data);
    }

    //untuk form input data dept
    function create()
    {
        return view('department.create');
    }

    //untuk simpan data dept
    function store(Request $request)
    {
        // $request->validate([
        //     'nik' => 'required',
        //     'name' => 'required|min:5',
        // ]);

        $messages = [
            'required' => 'The :attribute field is required yaaaa.',
        ];
        
        $rules = [
            'nama_department' => 'required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        
        //return $request->all(); GAK JADI

        //insert ke database
        $department                   = new Department;
        $department->nama_department    = $request->nama_department;
        $department->save();

        // return redirect('department');
        return redirect('department')->with('status','A New Data Department has created');
    }

    //untuk menampilkan form edit karyawan
    function edit($id)
    {
        $data['department'] = Department::where('id',$id)->first();
        return view('department.edit',$data);
    }

    //untuk menampilkan update
    function update(Request $request,$id)
    {

        $messages = [
            'required' => 'The :attribute field is required yaaaa.',
        ];
        
        $rules = [
            'nama_department' => 'required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        
        //return $request->all(); GAK JADI

        //ke database
        $department                         = Department::where('id',$id)->first();
        $department->nama_department        = $request->nama_department;
        $department->update();

        // return redirect('department');
        return redirect('department')->with('status','A Data Department has Updated');

    }

    //ngapus
    function destroy($nik)
    {
        $department   = Department::where('id',$id)->first();
        $department->delete();
        return redirect('department')->with('status','A Data Department Name '.$department->nama_department.' has DELETED');
    }
    
}
