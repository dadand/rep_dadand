<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users']=\DB::table('users')->paginate(3);
        return view('user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $password   = $request->password;
        $password   = Hash::make($request->password);
        $data       = ['name'=>$request->name,'password'=>$password,'email'=>$request->email];
        \DB::table('users')->insert($data);
        return redirect('user');
        // return $reques-all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = \DB::table('users')->where('id',$id)->first();
        return view('user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password=='')
        {
            $data       = ['name'=>$request->name,'email'=>$request->email];    
        }
        else
        {
            $password   = Hash::make($request->password);
            $data       = ['name'=>$request->name,'password'=>$password,'email'=>$request->email];
        }
        
        \DB::table('users')->where('id',$id)->update($data);
        return redirect('user');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('users')->where('id',$id)->delete();
        return redirect('user');        
    }
}
