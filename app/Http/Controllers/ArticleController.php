<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    function allArticle()
    {
        $data['articles']=[
            ['title'=>'Ronaldo Naik Haji','publish_date'=>'2019-10-30','category'=>'Olahraga'],
            ['title'=>'Peresmian Tol Merak','publish_date'=>'2019-11-30','category'=>'Otomotif'],
            ['title'=>'Ronaldo Uji Coba Tol Baru','publish_date'=>'2019-12-30','category'=>'Otomotif'],
        ];
        return view('list-article',$data);
    }   
    
    function detailArticle()
    {
        $data['articlenya'] = ['title'=>'Ronaldo Naik Haji Bersama Keluarga','publish_date'=>'2019-10-10'];
        return view('detail-article',$data);
    }
}
