<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('auth.login');
});

route::get('welcome',function()
{
    return 'Selamat Dataaaang';
});

route::get('uhuy',function()
{
    return 'alohaaaaa';
});

route::get('yuhu',function()
{
    return '<b>sukses</b> <br> selalu';
}
);

route::get('yuhu/{name}',function($name)
{
    return 'haaai '.$name;
});

route::get('welcome/{name}',function($name)
{
    //string
    $nama = 'Dadan';

    //integer
    $umur = 17;

    //boolean
    $sudahMenikah = true;

    //array
    $anak=['John','Doe'];

    //multi array
    $students = [
        ['nik'=>'TI18191234','name'=>'Yumna'],
        ['nik'=>'TI18193456','name'=>'Hanin']
    ];
    //return $students;

    //multi array UNTUK view di student.php
    $data['students'] = [
        ['nik'=>'TI18191234','name'=>'Yumna'],
        ['nik'=>'TI18193456','name'=>'Hanin']
    ];
    return view('student',$data);
   
}
);

Route::get('/article','ArticleController@allArticle');
Route::get('/article/1','ArticleController@detailArticle');


Route::get('employee/excel','EmployeeController@excel');
Route::get('employee/pdf','EmployeeController@pdf');
Route::resource('employee','EmployeeController');

// Route::get('employee','EmployeeController@index');
// Route::post('employee','EmployeeController@store');
// Route::get('employee/create','EmployeeController@create');
// Route::get('employee/{nik}/edit','EmployeeController@edit');
// Route::put('employee/{nik}','EmployeeController@update');
// Route::delete('employee/{nik}','EmployeeController@destroy');

Route::resource('user','UserController');


Route::get('department','DepartmentController@index');
Route::post('department','DepartmentController@store');
Route::get('department/create','DepartmentController@create');
Route::get('department/{id}/edit','DepartmentController@edit');
Route::put('department/{id}','DepartmentController@update');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
